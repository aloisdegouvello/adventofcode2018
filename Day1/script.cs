var lines = new List<string>();

for (var line = Console.ReadLine()
	; !string.IsNullOrEmpty(line)
	; line = Console.ReadLine()) {
	lines.Add(line);
}

Console.WriteLine(lines.Select(int.Parse).Sum());