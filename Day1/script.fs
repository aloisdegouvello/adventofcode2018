let rec readAllLines lines = 
    match System.Console.ReadLine() with
    | null | "" -> List.rev lines
    | s -> readAllLines (s::lines)

readAllLines []
|> Seq.map (fun x -> x |> int)
|> Seq.sum
|> printfn "%d"